package com.example.VEM.OKHttpCallBack;

import android.support.annotation.NonNull;

import com.example.VEM.EventBusEvent.MerchandiseListEvent;
import com.example.VEM.Model.RemoteMerchandise;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class MerchandiseListCallBack implements Callback{

    public MerchandiseListCallBack(){

    }

    @Override
    public void onFailure(@NonNull Call call,@NonNull IOException e){

    }

    @Override
    public void onResponse(@NonNull Call call,@NonNull Response response){
        if (response.isSuccessful() && response.body() != null){
            onEventBus(response);
        }
    }

    private void onEventBus(Response response){
        try {
            List<RemoteMerchandise> merchandiseList = createMerchandiseList(response.body().string());
            MerchandiseListEvent merchandiseListEvent = new MerchandiseListEvent(merchandiseList);
            EventBus.getDefault().post(merchandiseListEvent);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private List<RemoteMerchandise> createMerchandiseList(String response){
        Type type = new TypeToken<List<RemoteMerchandise>>(){}.getType();
        return new Gson().fromJson(response,type);
    }

}
