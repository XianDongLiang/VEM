package com.example.VEM.OKHttpCallBack;

import com.example.VEM.EventBusEvent.AdvertisementListEvent;
import com.example.VEM.Model.RemoteAdvertisement;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class AdvertisementListCallBack implements Callback {

    public AdvertisementListCallBack(){

    }

    @Override
    public void onFailure(Call call, IOException e){

    }

    @Override
    public void onResponse(Call call, Response response){
        if (response.isSuccessful() && response.body() != null){
            onEventBus(response);
        }
    }

    private void onEventBus(Response response){
        try{
            List<RemoteAdvertisement> advertisementList = createAdvertisementList(response.body().string());
            AdvertisementListEvent advertisementListEvent = new AdvertisementListEvent(advertisementList);
            EventBus.getDefault().post(advertisementListEvent);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private List<RemoteAdvertisement> createAdvertisementList(String response){
        Type type = new TypeToken<List<RemoteAdvertisement>>(){}.getType();
        return new Gson().fromJson(response,type);
    }
}
