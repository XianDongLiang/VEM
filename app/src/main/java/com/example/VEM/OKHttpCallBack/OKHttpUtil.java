package com.example.VEM.OKHttpCallBack;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;

public class OKHttpUtil {

    private OKHttpUtil() {

    }

    private static OKHttpUtil INSTANCE = null;
    private static OkHttpClient okHttpClient = null;

    public synchronized static OKHttpUtil getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new OKHttpUtil();
            if (okHttpClient == null) {
                okHttpClient = new OkHttpClient();
            }
        }
        return INSTANCE;
    }

    /**/
    public void requestServer(String address,Callback callback) {
        final Request request = new Request.Builder().url(address).build();
        final Call call = okHttpClient.newCall(request);
        call.enqueue(callback);
    }

}