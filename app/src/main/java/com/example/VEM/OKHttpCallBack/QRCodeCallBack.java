package com.example.VEM.OKHttpCallBack;

import android.support.annotation.NonNull;

import com.example.VEM.EventBusEvent.QRCodeLinkEvent;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class QRCodeCallBack implements Callback{

    private String method;

    public QRCodeCallBack(String method){
        this.method = method;
    }

    @Override
    public void onFailure(@NonNull Call call, @NonNull IOException e){

    }

    @Override
    public void onResponse(@NonNull Call call,@NonNull Response response){
        if (response.isSuccessful() && response.body() != null){
            try{
                JSONObject codeObject = new JSONObject(response.body().string());
                String link = codeObject.getString("Link");
                QRCodeLinkEvent linkEvent = new QRCodeLinkEvent(method,link);
                EventBus.getDefault().post(linkEvent);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

}
