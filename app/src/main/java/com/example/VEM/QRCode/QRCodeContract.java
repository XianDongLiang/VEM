package com.example.VEM.QRCode;

import android.graphics.Bitmap;

public interface QRCodeContract {
    Bitmap generateQRCode(String transactionString, int width,int height);
}
