package com.example.VEM.QRCode;

import android.graphics.Bitmap;
import android.graphics.Color;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import java.util.Hashtable;

public class QRCode implements QRCodeContract{

    @Override
    public Bitmap generateQRCode(String transactionString,int width,int height){
        if(!transactionString.isEmpty()){
            try {
                Hashtable<EncodeHintType,String> hints = new Hashtable<>();
                hints.put(EncodeHintType.CHARACTER_SET,"UTF-8");
                BitMatrix matrix = new QRCodeWriter().encode(transactionString, BarcodeFormat.QR_CODE,width,height,hints);
                int[] pixels = new int[width*height];
                for (int y=0 ; y<height ; y++){
                    for (int x=0 ; x<width ; x++){
                        if (matrix.get(x,y)){
                            pixels[y*width+x] = Color.BLACK;
                        }else {
                            pixels[y*height+x] = Color.WHITE;
                        }
                    }
                }
                Bitmap bitmap = Bitmap.createBitmap(width,height, Bitmap.Config.ARGB_8888);
                bitmap.setPixels(pixels,0,width,0,0,width,height);
                return bitmap;
            }catch (WriterException e){
                e.printStackTrace();
                return null;
            }
        }else {
            return null;
        }

    }

}
