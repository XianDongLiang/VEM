package com.example.VEM.Main.Merchandise.Design_debug;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class MerchandiseDecoration extends RecyclerView.ItemDecoration {

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state){
        outRect.left = 10;
        outRect.top = 12;
    }

}
