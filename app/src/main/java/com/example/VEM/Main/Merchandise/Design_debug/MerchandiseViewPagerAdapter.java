package com.example.VEM.Main.Merchandise.Design_debug;

import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

public class MerchandiseViewPagerAdapter extends PagerAdapter{

    private List<View> viewList;

    public MerchandiseViewPagerAdapter(List<View> viewList){
        this.viewList = viewList;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position,@NonNull Object object){
        container.removeView(viewList.get(position));
    }

    @Override
    @NonNull
    public Object instantiateItem(@NonNull ViewGroup container, int position){
        container.addView(viewList.get(position));
        return viewList.get(position);
    }

    @Override
    public int getCount(){
        if (viewList == null){
            return 0;
        }else {
            return viewList.size();
        }
    }

    @Override
    public boolean isViewFromObject(@NonNull View view,@NonNull Object object){
        return view == object;
    }

    @Override
    public int getItemPosition(Object object){
        return PagerAdapter.POSITION_NONE;
    }
}
