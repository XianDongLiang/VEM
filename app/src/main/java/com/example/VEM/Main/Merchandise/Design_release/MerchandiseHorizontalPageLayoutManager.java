package com.example.VEM.Main.Merchandise.Design_release;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.View;

public class MerchandiseHorizontalPageLayoutManager extends RecyclerView.LayoutManager implements MerchandisePageDecoration {

    /*X轴上的偏移量*/
    private int offsetX;
    /*Y轴上的偏移量*/
    private int offsetY;
    /**/
    private int totalWidth;
    /**/
    private int totalHeight;
    /*页面行数*/
    private int rows;
    /*页面列数*/
    private int columns;
    /*页面面积*/
    private int pageSize;
    private int onePageSize;
    private int itemWidth;
    private int itemHeight;
    /**/
    private int itemWidthUsed;
    /**/
    private int itemHeightUsed;
    /**/
    private SparseArray<Rect> allItemFrames = new SparseArray<>();

    @Override
    public RecyclerView.LayoutParams generateDefaultLayoutParams(){
        return null;
    }

    public MerchandiseHorizontalPageLayoutManager(int rows, int columns){
        this.rows = rows;
        this.columns = columns;
        onePageSize = rows * columns;
    }

    /*查询当前页面是否支持水平滚动*/
    @Override
    public boolean canScrollHorizontally(){
        /*true  当前页面支持水平滑动*/
        /*false 当前页面不支持水平滑动*/
        return true;
    }

    /*
    *   在屏幕坐标中按dx像素水平滚动并返回行进的距离
    *   dx  滾動距離（以像素為單位）當滾動位置接近右側時，X增加
    *   Recycler    用於獲取位置的潛在緩存視圖
    *   RecyclerView    瞬態
    * */
    @Override
    public int scrollHorizontallyBy(int dx, RecyclerView.Recycler recycler,RecyclerView.State state){
        /*暂时分离并废弃所有当前附加的子视图*/
        detachAndScrapAttachedViews(recycler);
        /*x等于偏移量加滑动距离*/
        int x = offsetX + dx;

        int result = dx;
        /**/
        if (x > totalWidth){
            result = totalWidth - offsetX;
        }else if (x < 0){
            result = 0 - offsetX;
        }
        offsetX = offsetX + result;
        /*使用水平轴上的dx像素偏移附加到父RecyclerView的所有子视图*/
        offsetChildrenHorizontal(-result);
        recycleAndFillItems(recycler,state);

        return result;
    }

    private void recycleAndFillItems(RecyclerView.Recycler recycler, RecyclerView.State state) {

        /*如果RecyclerView处于预布局步骤，并且它具有RecyclerView.LayoutManager布局项目，它们将位于一组预测项目动画的开头，则返回true*/
        if (state.isPreLayout()) {
            return;
        }
        /*绘制矩形*/
        Rect displayRect = new Rect(getPaddingLeft() + offsetX,getPaddingTop(),getWidth() - getPaddingLeft() - getPaddingRight() + offsetX,getHeight() - getPaddingTop() - getPaddingBottom());
        /*绘制子矩形*/
        Rect childRect = new Rect();
        /*
        * getChildCount()
        * Return the current number of child views attached to the parent RecyclerView
        * 返回附加到父RecyclerView的当前子视图数
        * */
        for (int i = 0; i < getChildCount(); i++) {
            /*
            * getChildAt()
            * Return the child view at the given index
            * 返回给定索引处的子视图
            * */
            View child = getChildAt(i);
            /*
            * getDecoratedLeft(View child)
            * Returns the left edge of the given child view within its parent, offset by any applied ItemDecorations
            * 返回给定子视图在其父视图中的左边缘，由任何应用的ItemDecorations偏移
            * */
            childRect.left = getDecoratedLeft(child);
            childRect.top = getDecoratedTop(child);
            childRect.right = getDecoratedRight(child);
            childRect.bottom = getDecoratedBottom(child);
            /**/
            if (!Rect.intersects(displayRect,childRect)) {
                removeAndRecycleView(child, recycler);
            }
        }

        for (int i = 0; i < getItemCount(); i++) {
            if (Rect.intersects(displayRect, allItemFrames.get(i))) {
                View view = recycler.getViewForPosition(i);
                addView(view);
                measureChildWithMargins(view, itemWidthUsed, itemHeightUsed);
                Rect rect = allItemFrames.get(i);
                layoutDecorated(view, rect.left - offsetX, rect.top, rect.right - offsetX, rect.bottom);
            }
        }

    }

    /*计算可用的宽度*/
    private int getUsableWidth() {
        return getWidth() - getPaddingLeft() - getPaddingRight();
    }
    /*计算可用的高度*/
    private int getUsableHeight() {
        return getHeight() - getPaddingTop() - getPaddingBottom();
    }

    @Override
    public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
        /*如果子视图的数目为0，则从recycler中删除所有的视图*/
        if (getItemCount() == 0) {
            removeAndRecycleAllViews(recycler);
            return;
        }
        if (state.isPreLayout()) {
            return;
        }
        //获取每个Item的平均宽高
        itemWidth = getUsableWidth()/columns;
        itemHeight = getUsableHeight()/rows;

        //计算宽高已经使用的量，主要用于后期测量
        itemWidthUsed = (columns - 1) * itemWidth;
        itemHeightUsed = (rows - 1) * itemHeight;

        //计算总的页数
        //pageSize = state.getItemCount() / onePageSize + (state.getItemCount() % onePageSize == 0 ? 0 : 1);
        computePageSize(state);
        //计算可以横向滚动的最大值
        totalWidth = (pageSize - 1) * getWidth();

        //分离view
        detachAndScrapAttachedViews(recycler);

        int count = getItemCount();
        for (int p = 0; p < pageSize; p++) {
            for (int r = 0; r < rows; r++) {
                for (int c = 0; c < columns; c++) {
                    int index = p * onePageSize + r * columns + c;
                    if (index == count) {
                        //跳出多重循环
                        c = columns;
                        r = rows;
                        p = pageSize;
                        break;
                    }

                    View view = recycler.getViewForPosition(index);
                    addView(view);
                    //测量item
                    measureChildWithMargins(view,itemWidthUsed,itemHeightUsed);

                    int width = getDecoratedMeasuredWidth(view);
                    int height = getDecoratedMeasuredHeight(view);
                    //记录显示范围
                    Rect rect = allItemFrames.get(index);
                    if (rect == null) {
                        rect = new Rect();
                    }
                    int x = p * getUsableWidth() + c * itemWidth;
                    int y = r * itemHeight;
                    rect.set(x, y, width + x, height + y);
                    allItemFrames.put(index, rect);

                }
            }
            //每一页循环以后就回收一页的View用于下一页的使用
            removeAndRecycleAllViews(recycler);
        }

        recycleAndFillItems(recycler, state);
    }

    private void computePageSize(RecyclerView.State state) {
        pageSize = state.getItemCount() / onePageSize + (state.getItemCount() % onePageSize == 0 ? 0 : 1);
    }

    @Override
    public void onDetachedFromWindow(RecyclerView view, RecyclerView.Recycler recycler) {
        super.onDetachedFromWindow(view, recycler);
        offsetX = 0;
        offsetY = 0;
    }

    @Override
    public boolean isLastRow(int index) {
        if (index >= 0 && index < getItemCount()) {
            int indexOfPage = index % onePageSize;
            indexOfPage++;
            if (indexOfPage > (rows - 1) * columns && indexOfPage <= onePageSize) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isLastColumn(int position) {
        if (position >= 0 && position < getItemCount()) {
            position++;
            if (position % columns == 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isLastPage(int position) {
        position++;
        return position % onePageSize == 0;
    }

    @Override
    public int computeHorizontalScrollRange(RecyclerView.State state) {
        computePageSize(state);
        return pageSize * getWidth();
    }

    @Override
    public int computeHorizontalScrollOffset(RecyclerView.State state) {
        return offsetX;
    }

    @Override
    public int computeHorizontalScrollExtent(RecyclerView.State state) {
        return getWidth();
    }
}
