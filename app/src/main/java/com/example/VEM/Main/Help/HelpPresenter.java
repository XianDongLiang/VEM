package com.example.VEM.Main.Help;

public class HelpPresenter implements HelpContract.HelpPresenter{

    private HelpContract.HelpView helpView;

    public HelpPresenter(HelpContract.HelpView helpView){
        this.helpView = helpView;
        this.helpView.initHelpPresenter(this);
    }

}
