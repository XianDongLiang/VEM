package com.example.VEM.Main.Activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.VEM.Constant.BaseFragment;
import com.example.VEM.R;

public class ActivityFragment extends BaseFragment implements ActivityContract.ActivitiesView{

    public ActivityFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fg_main_activity, container, false);

        return view;
    }

    @Override
    public void initActivityPresenter(){

    }

}
