package com.example.VEM.Main.Merchandise.Design_debug;

import android.support.v4.view.ViewPager;
import android.widget.LinearLayout;

import com.example.VEM.R;

public class MerchandisePageChangeListener implements ViewPager.OnPageChangeListener {

    private LinearLayout indicator;
    private int currentIndex;

    public MerchandisePageChangeListener(LinearLayout indicator, int currentIndex){
        this.indicator = indicator;
        this.currentIndex = currentIndex;
    }

    @Override
    public void onPageSelected(int position) {
        indicator.getChildAt(currentIndex).findViewById(R.id.itempage_indicator_item)
                .setBackgroundResource(R.drawable.border_main_merchandise_indicator_unselect);
        indicator.getChildAt(position).findViewById(R.id.itempage_indicator_item)
                .setBackgroundResource(R.drawable.border_main_merchandise_indicator_select);
        currentIndex = position;
    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {

    }

    @Override
    public void onPageScrollStateChanged(int arg0) {

    }
}
