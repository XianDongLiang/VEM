package com.example.VEM.Main.Main;

public interface MainContract {
    interface MainView{
        void initMainPresenter(MainPresenter mainPresenter);
    }
    interface MainPresenter{
        void installSerialPort();
        void destroy();
    }
}
