package com.example.VEM.Main.Advertisement;

import com.example.VEM.Constant.Constant;
import com.example.VEM.OKHttpCallBack.AdvertisementListCallBack;
import com.example.VEM.OKHttpCallBack.OKHttpUtil;

public class AdvertisementPresenter implements AdvertisementContract.AdvertisementPresenter {

    private AdvertisementContract.AdvertisementView advertisementView;

    public AdvertisementPresenter(AdvertisementContract.AdvertisementView advertisementView){
        this.advertisementView = advertisementView;
        this.advertisementView.initAdvertisementPresenter(this);
    }

    @Override
    public void loadRemoteAdvertisementList(){
        AdvertisementListCallBack callBack = new AdvertisementListCallBack();
        OKHttpUtil.getInstance().requestServer(Constant.ADVERTISEMENT_DOWNLOAD_URL,callBack);
    }

}
