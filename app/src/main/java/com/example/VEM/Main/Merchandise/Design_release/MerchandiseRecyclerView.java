package com.example.VEM.Main.Merchandise.Design_release;

import android.content.Context;
import android.graphics.Canvas;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.Scroller;

public class MerchandiseRecyclerView extends RecyclerView {

    private Scroller scroller;
    private int startX;
    private int startY;

    public MerchandiseRecyclerView(Context context) {
        this(context, null);
    }

    public MerchandiseRecyclerView(Context context,@Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MerchandiseRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setOnFlingListener(new MerchandiseOnFlingListener());
        scroller = new Scroller(context);
    }

    class MerchandiseOnFlingListener extends OnFlingListener {

        @Override
        public boolean onFling(int velocityX, int velocityY) {
            int dy;
            if (velocityY <0) {
                dy = 0 - startY;
            } else {
                dy = getHeight() - startY;
            }
            scroller.startScroll(0, startY, 0, dy, 500);
            invalidate();
            return true;
        }
    }

    @Override
    public void onDraw(Canvas c) {
        super.onDraw(c);
        if(scroller.computeScrollOffset()){
            int x = scroller.getCurrX();
            int y = scroller.getCurrY();
            int dy=y-startY;
            scrollBy(0,dy);
            startY=y;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        if (e.getAction() == MotionEvent.ACTION_UP) {
            startX = (int) e.getX();
            startY = (int) e.getY();
        }
        return super.onTouchEvent(e);
    }
}
