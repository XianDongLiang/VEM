package com.example.VEM.Main.Advertisement;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.VEM.R;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ScrollViewPagerAdapter extends PagerAdapter {

    private Map<Integer,View> advertisementMap = new HashMap<>();
    private List<String> localPathList;
    private ImageView advertisement;
    private Context context;

    public ScrollViewPagerAdapter(Context context,List<String> localPathList){
        this.context = context;
        this.localPathList = localPathList;
    }

    @Override
    public int getCount(){
        return localPathList == null ? 0:localPathList.size();
    }

    @NonNull
    public Object instantiateItem(ViewGroup container, int position) {
        if (advertisementMap.containsKey(position)){
            container.addView(advertisementMap.get(position));
            return advertisementMap.get(position);
        }else {
            View view = LayoutInflater.from(context).inflate(R.layout.single_main_advertisement,container,false);
            advertisement = view.findViewById(R.id.img_main_advertisement_single);
            Glide.with(context).load(localPathList.get(position)).into(advertisement);
            container.addView(view);
            advertisementMap.put(position,view);
            return view;
        }
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object){
        return (view == object);
    }

}
