package com.example.VEM.Main.Main;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.VEM.Main.Activity.ActivityFragment;
import com.example.VEM.Constant.BaseFragment;
import com.example.VEM.Main.Merchandise.MerchandiseFragment;
import com.example.VEM.Main.Help.HelpFragment;

public class MainFragmentStatePagerAdapter extends FragmentStatePagerAdapter {

    private static final int NUMBER = 3;

    private static final int PAGE_FIRST = 0;
    private static final int PAGE_SECOND = 1;
    private static final int PAGE_THIRD = 2;

    public MainFragmentStatePagerAdapter(FragmentManager fragmentManager){
        super(fragmentManager);
    }

    @Override
    public Fragment getItem(int position){
        BaseFragment baseFragment = null;
        switch (position){
            case PAGE_FIRST:baseFragment = new MerchandiseFragment();break;
            case PAGE_SECOND:baseFragment = new HelpFragment();break;
            case PAGE_THIRD:baseFragment = new ActivityFragment();break;
            default:break;
        }
        return baseFragment;
    }

    @Override
    public int getCount(){
        return NUMBER;
    }
}
