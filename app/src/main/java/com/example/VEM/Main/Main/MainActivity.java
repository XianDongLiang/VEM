package com.example.VEM.Main.Main;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.RadioGroup;

import com.example.VEM.R;

public class MainActivity extends AppCompatActivity implements MainContract.MainView,RadioGroup.OnCheckedChangeListener{

    private MainContract.MainPresenter mainPresenter;

    private ViewPager mainViewPager;

    private static final int PAGE_FIRST = 0;
    private static final int PAGE_SECOND = 1;
    private static final int PAGE_THIRD = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new MainPresenter(this);
        /*TODO:串口单例模式初始化*/
        mainPresenter.installSerialPort();
        initViewPager();
        initNavigationGroup();
    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        mainPresenter.destroy();
        mainPresenter = null;
    }

    private void initNavigationGroup(){
        RadioGroup mainNavigation = findViewById(R.id.rg_main_navigation_pressed);
        mainNavigation.check(R.id.rb_main_navigation_merchandise_pressed);
        mainNavigation.setOnCheckedChangeListener(this);
    }

    private void initViewPager(){
        MainFragmentStatePagerAdapter mainFragmentStatePagerAdapter = new MainFragmentStatePagerAdapter(getSupportFragmentManager());
        mainViewPager = findViewById(R.id.vp_main_switch_change);
        mainViewPager.setAdapter(mainFragmentStatePagerAdapter);
        mainViewPager.setCurrentItem(0);
    }

    @Override
    public void onCheckedChanged(RadioGroup group,int checkedId){
        switch(checkedId){
            case R.id.rb_main_navigation_merchandise_pressed:{
                mainViewPager.setCurrentItem(PAGE_FIRST);
            }break;
            case R.id.rb_main_navigation_help_pressed:{
                mainViewPager.setCurrentItem(PAGE_SECOND);
            }break;
            case R.id.rb_main_navigation_activity_pressed:{
                mainViewPager.setCurrentItem(PAGE_THIRD);
            }break;
            default:{

            }break;
        }
    }

    @Override
    public void initMainPresenter(MainContract.MainPresenter mainPresenter){
        this.mainPresenter = mainPresenter;
    }

}
