package com.example.VEM.Main.Merchandise;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.VEM.EventBusEvent.MerchandiseListEvent;
import com.example.VEM.Main.Merchandise.Design_release.MerchandiseHorizontalPageLayoutManager;
import com.example.VEM.Main.Merchandise.Design_release.MerchandisePagingScrollHelper;
import com.example.VEM.Model.RemoteMerchandise;
import com.example.VEM.Constant.BaseFragment;
import com.example.VEM.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

public class MerchandiseFragment extends BaseFragment implements MerchandisePagingScrollHelper.onPageChangeListener,MerchandiseContract.MerchandiseView{

    private static final int PAGE_SIZE = 10;
    private int pageCount;
    private RecyclerView recyclerView;
    private List<RemoteMerchandise> merchandiseList;
    private LinearLayout indicator;
    private MerchandiseContract.MerchandisePresenter merchandisePresenter;

    public MerchandiseFragment() {

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fg_main_merchandise, container, false);
        new MerchandisePresenter(this);
        merchandisePresenter.loadRemoteMerchandiseList();
        initItemPageView(view);
        return view;
    }

    @Override
    public void onStart(){
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop(){
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void initMerchandisePresenter(MerchandiseContract.MerchandisePresenter merchandisePresenter){
        this.merchandisePresenter = merchandisePresenter;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MerchandiseListEvent event){
        merchandiseList = event.obtainMerchandiseList();
        initPageCount();
        initItemPageItemList();
    }

    @Override
    public void onPageChange(int index) {

    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        merchandisePresenter.destroy();
        merchandisePresenter = null;
    }

    private void initItemPageView(View view){
        recyclerView = view.findViewById(R.id.rv_main_merchandise_display_desc);
        indicator = view.findViewById(R.id.ll_main_merchandise_indicator_desc);
    }

    private void initPageCount(){
        pageCount = (int)Math.ceil(merchandiseList.size()*1.0/PAGE_SIZE);
    }

    private void initItemPageItemList(){
        MerchandiseRecycleViewAdapter adapter = new MerchandiseRecycleViewAdapter(getContext(),merchandiseList);
        /*自定义LayoutManager改变recyclerView的布局*/
        MerchandiseHorizontalPageLayoutManager horizontalPageLayoutManager = new MerchandiseHorizontalPageLayoutManager(2,5);
        /*自定义ItemDecoration改变Item布局样式参数*/
        //MerchandisePagingItemDecoration pagingItemDecoration = new MerchandisePagingItemDecoration(getContext(),horizontalPageLayoutManager);
        MerchandisePagingScrollHelper pagingScrollHelper = new MerchandisePagingScrollHelper();
        pagingScrollHelper.setUpRecycleView(recyclerView);
        recyclerView.setHorizontalScrollBarEnabled(true);
        recyclerView.setLayoutManager(horizontalPageLayoutManager);
        /*为ItemView添加布局样式参数*/
        //recyclerView.addItemDecoration(pagingItemDecoration);
        recyclerView.setAdapter(adapter);

    }

    private void initIndicator() {
        for (int i = 0;i<pageCount;i++) {
            indicator.addView(getLayoutInflater().inflate(R.layout.indicator_main_merchandise,null));
        }
        indicator.getChildAt(0).findViewById(R.id.itempage_indicator_item).setBackgroundResource(R.drawable.border_main_merchandise_indicator_select);
        //viewPager.addOnPageChangeListener(new MerchandisePageChangeListener(indicator,0));
    }

}
