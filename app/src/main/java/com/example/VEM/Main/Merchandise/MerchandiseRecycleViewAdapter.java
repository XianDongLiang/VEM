package com.example.VEM.Main.Merchandise;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.example.VEM.Model.RemoteMerchandise;
import com.example.VEM.R;

import java.util.List;

public class MerchandiseRecycleViewAdapter extends RecyclerView.Adapter<MerchandiseViewHolder> {

    private Context context;
    private List<RemoteMerchandise> merchandiseList;
    private int pageIndex;
    private int pageSize;

    public MerchandiseRecycleViewAdapter(Context context, List<RemoteMerchandise> merchandiseList, int index, int pageSize){
        this.context = context;
        this.merchandiseList = merchandiseList;
        this.pageIndex = index;
        this.pageSize = pageSize;
    }

    public MerchandiseRecycleViewAdapter(Context context,List<RemoteMerchandise> merchandiseList){
        this.context = context;
        this.merchandiseList = merchandiseList;
    }

    @Override
    @NonNull
    public MerchandiseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        View itemView = LayoutInflater.from(context).inflate(R.layout.single_main_merchandise,parent,false);
        return new MerchandiseViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MerchandiseViewHolder merchandiseViewHolder, int position){
        Glide.with(context)
                .load(merchandiseList.get(position).obtainImageFolderPath())
                .into(merchandiseViewHolder.merchandiseImage);
        merchandiseViewHolder.merchandiseImage.setOnClickListener(new MerchandiseOnClickListener(merchandiseList.get(position),context));
        merchandiseViewHolder.merchandisePrice.setText(String.valueOf(merchandiseList.get(position).obtainPrice()));
    }

    @Override
    public int getItemCount(){
//        return merchandiseList.size()>(pageIndex+1)*pageSize?pageSize:(merchandiseList.size()-pageIndex*pageSize);
        return merchandiseList.size();
    }

//    @Override
//    public long getItemId(int position){
//        return (position+pageIndex*pageSize);
//    }

}
