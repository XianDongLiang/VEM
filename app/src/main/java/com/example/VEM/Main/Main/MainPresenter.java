package com.example.VEM.Main.Main;

import com.example.VEM.Model.ApplicationThreadPool;

public class MainPresenter implements MainContract.MainPresenter{

    private MainContract.MainView mainView;

    public MainPresenter(MainContract.MainView mainView){
        this.mainView = mainView;
        this.mainView.initMainPresenter(this);
    }

    @Override
    public void installSerialPort(){
        ApplicationThreadPool.getInstance().installAllConfigs();
    }

    @Override
    public void destroy(){
        mainView = null;
    }
}
