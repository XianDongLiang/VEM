package com.example.VEM.Main.Merchandise;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.example.VEM.Bargain.BargainActivity;
import com.example.VEM.EventBusEvent.MerchandiseSingleEvent;
import com.example.VEM.Model.RemoteMerchandise;

import org.greenrobot.eventbus.EventBus;

public class MerchandiseOnClickListener implements View.OnClickListener {

    private RemoteMerchandise merchandise;
    private Context context;

    public MerchandiseOnClickListener(RemoteMerchandise merchandise, Context context){
        this.merchandise = merchandise;
        this.context = context;
    }

    @Override
    public void onClick(View view){
        Intent intent = new Intent(context,BargainActivity.class);
        MerchandiseSingleEvent event = new MerchandiseSingleEvent(merchandise);
        EventBus.getDefault().postSticky(event);
        context.startActivity(intent);
    }

}
