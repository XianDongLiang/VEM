package com.example.VEM.Main.Merchandise;

public interface MerchandiseContract {
    interface MerchandiseView{
        void initMerchandisePresenter(MerchandisePresenter merchandisePresenter);
    }
    interface MerchandisePresenter{
        void loadRemoteMerchandiseList();
        void destroy();
    }
}
