package com.example.VEM.Main.Merchandise;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.VEM.R;

public class MerchandiseViewHolder extends RecyclerView.ViewHolder{

    public ImageView merchandiseImage;
    public TextView merchandisePrice;

    public MerchandiseViewHolder(View itemView){
        super(itemView);
        merchandiseImage = itemView.findViewById(R.id.img_main_merchandise_single_desc);
        merchandisePrice = itemView.findViewById(R.id.tv_main_merchandise_single_desc);
    }

}
