package com.example.VEM.Main.Merchandise.Design_release;

public interface MerchandisePageDecoration {
    /*判断是否是最后一行*/
    boolean isLastRow(int position);
    /*判断是否为最后一列*/
    boolean isLastColumn(int position);
    /*判断是否为最后一页*/
    boolean isLastPage(int position);
}
