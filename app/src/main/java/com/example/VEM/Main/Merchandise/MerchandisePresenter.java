package com.example.VEM.Main.Merchandise;

import com.example.VEM.Constant.Constant;
import com.example.VEM.OKHttpCallBack.MerchandiseListCallBack;
import com.example.VEM.OKHttpCallBack.OKHttpUtil;

public class MerchandisePresenter implements MerchandiseContract.MerchandisePresenter {

    private MerchandiseContract.MerchandiseView merchandiseView;

    public MerchandisePresenter(MerchandiseContract.MerchandiseView itemPageView){
        this.merchandiseView = itemPageView;
        this.merchandiseView.initMerchandisePresenter(this);
    }

    @Override
    public void loadRemoteMerchandiseList(){
        MerchandiseListCallBack merchandiseListCallBack = new MerchandiseListCallBack();
        OKHttpUtil.getInstance().requestServer(Constant.MERCHANDISE_DOWNLOAD_URL, merchandiseListCallBack);
    }

    @Override
    public void destroy(){
        merchandiseView = null;
    }

}


