package com.example.VEM.Main.Help;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.VEM.Constant.BaseFragment;
import com.example.VEM.R;

public class HelpFragment extends BaseFragment implements HelpContract.HelpView{

    private HelpContract.HelpPresenter usingHelpPresenter;

    public HelpFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fg_main_help, container, false);
        new HelpPresenter(this);
        return view;
    }

    @Override
    public void initHelpPresenter(HelpContract.HelpPresenter usingHelpPresenter){
        this.usingHelpPresenter = usingHelpPresenter;
    }
}
