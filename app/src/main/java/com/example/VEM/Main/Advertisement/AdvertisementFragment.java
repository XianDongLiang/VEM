package com.example.VEM.Main.Advertisement;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.VEM.EventBusEvent.AdvertisementListEvent;
import com.example.VEM.Constant.BaseFragment;
import com.example.VEM.Model.RemoteAdvertisement;
import com.example.VEM.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;

public class AdvertisementFragment extends BaseFragment implements AdvertisementContract.AdvertisementView{

    private AdvertisementContract.AdvertisementPresenter advertisementPresenter;
    private AutoScrollViewPager autoScrollViewPager;

    public AdvertisementFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fg_main_advertisement, container, false);
        new AdvertisementPresenter(this);
        advertisementPresenter.loadRemoteAdvertisementList();
        initAdvertisementView(view);
        return view;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(AdvertisementListEvent event){
        List<RemoteAdvertisement> advertisementList = event.obtainAdvertisementList();
        List<String> remoteAdvertisementPath = new ArrayList<>();
        for (RemoteAdvertisement advertisement:advertisementList){
            remoteAdvertisementPath.add(advertisement.obtainAdvertisementURL());
        }
        ScrollViewPagerAdapter scrollViewPagerAdapter = new ScrollViewPagerAdapter(getContext(),remoteAdvertisementPath);
        autoScrollViewPager.setAdapter(scrollViewPagerAdapter);
    }

    @Override
    public void initAdvertisementPresenter(AdvertisementContract.AdvertisementPresenter advertisementPresenter){
        this.advertisementPresenter = advertisementPresenter;
    }

    /*初始化AutoScrollViewPager并设置参数*/
    private void initAdvertisementView(View view){
        autoScrollViewPager = view.findViewById(R.id.vp_advertisement_autoScrollViewPager);
        setAutoScrollViewPagerConfigs(autoScrollViewPager);
    }

    private void setAutoScrollViewPagerConfigs(AutoScrollViewPager autoScrollViewPager){
        autoScrollViewPager.setInterval(3000);
        autoScrollViewPager.setDirection(AutoScrollViewPager.RIGHT);
        autoScrollViewPager.setCycle(true);
        autoScrollViewPager.setScrollDurationFactor(20);
        autoScrollViewPager.setSlideBorderMode(AutoScrollViewPager.SLIDE_BORDER_MODE_CYCLE);
        autoScrollViewPager.setBorderAnimation(false);
    }

    @Override
    public void onStart(){
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onResume(){
        super.onResume();
        autoScrollViewPager.startAutoScroll();
    }

    @Override
    public void onPause(){
        super.onPause();
        autoScrollViewPager.stopAutoScroll();
    }

    @Override
    public void onStop(){
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();

    }
}
