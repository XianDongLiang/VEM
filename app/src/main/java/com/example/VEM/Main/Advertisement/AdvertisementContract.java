package com.example.VEM.Main.Advertisement;

public interface AdvertisementContract {
    interface AdvertisementView{
        void initAdvertisementPresenter(AdvertisementPresenter advertisementPresenter);
    }
    interface AdvertisementPresenter{
        /*获取服务器广告JSON列表*/
        void loadRemoteAdvertisementList();
    }
}
