package com.example.VEM.Model;

public class RemoteMerchandise{

    /*识别售货机唯一UID*/
    private String VEMUID;
    /*识别商品唯一UID*/
    private String merchandiseUID;
    /*商品货道*/
    private int merchandiseSlot;
    /*商品剩余数量*/
    private int merchandiseRemain;
    /*商品价格*/
    private double merchandisePrice;
    /*商品图片名称*/
    private String merchandiseImageName;
    /*商品名称*/
    private String merchandiseName;
    /*商品图片路径*/
    private String merchandiseFolderPath;

    public RemoteMerchandise(){

    }

    /*返回售货机唯一UID*/
    public String obtainVEMUID(){
        return VEMUID;
    }
    /*返回商品唯一UID*/
    public String obtainMerchandiseUID(){
        return merchandiseUID;
    }
    /*返回商品货道编号*/
    public int obtainSlot(){
        return merchandiseSlot;
    }
    /*返回商品剩余数量*/
    public int obtainRemain(){
        return merchandiseRemain;
    }
    /*返回商品价格*/
    public double obtainPrice(){
        return merchandisePrice;
    }
    /*返回商品图片名称*/
    public String obtainImageName(){
        return merchandiseImageName;
    }
    /*返回商品名称*/
    public String obtainMerchandiseName(){
        return merchandiseName;
    }
    /*返回商品图片路径*/
    public String obtainImageFolderPath(){
        return merchandiseFolderPath;
    }

}
