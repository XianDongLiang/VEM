package com.example.VEM.Model;

public class OriginalByteData {
    /*数据的长度*/
    private int length;
    /*串口数据*/
    private byte[] data;
    /*初始化数据长度*/
    public void installLength(int length){
        this.length = length;
    }
    /*初始化数据*/
    public void installData(byte[] data){
        this.data = data;
    }
    /*获取数据*/
    public byte[] obtainData(){
        return data;
    }
    /*获取数据长度*/
    public int obtainLength(){
        return length;
    }
}
