package com.example.VEM.Model;

public class RemoteAdvertisement {

    /*图片唯一识别码*/
    private String advertisementUID;
    /*图片名称*/
    private String advertisementName;
    /*图片下载地址*/
    private String advertisementURL;

    public RemoteAdvertisement(){

    }

    /*返回广告路径*/
    public String obtainAdvertisementURL(){
        return advertisementURL;
    }
    /*返回广告名称*/
    public String obtainAdvertisementName(){
        return advertisementName;
    }
    /*返回广告唯一识别UID*/
    public String obtainAdvertisementUID(){
        return advertisementUID;
    }

}
