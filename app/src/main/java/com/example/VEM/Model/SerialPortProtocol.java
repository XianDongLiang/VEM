package com.example.VEM.Model;

public class SerialPortProtocol {
    /*帧开始位*/
    private final byte START = (byte)0xAA;
    /*设备地址*/
    private byte deviceAddress;
    /*功能码*/
    private byte functionCode_major;
    /*扩展功能码*/
    private byte functionCode_expand;
    /*驱动地址*/
    private byte driveAddress;
    /*数据存储*/
    private byte[] data;
    /*CRC高位*/
    private byte CRC_H;
    /*CRC低位*/
    private byte CRC_L;
    /*帧结束位*/
    private final byte END = (byte)0xFF;

}
