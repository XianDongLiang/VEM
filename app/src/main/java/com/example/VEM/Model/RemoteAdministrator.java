package com.example.VEM.Model;

public class RemoteAdministrator {

    /*管理员账户*/
    private String account;
    /*管理员密码*/
    private String password;

    public RemoteAdministrator(String account,String password){
        this.account = account;
        this.password = password;
    }

    /*返回管理员账户*/
    public String obtainAccount() {
        return account;
    }
    /*返回管理员密码*/
    public String obtainPassword() {
        return password;
    }

}
