package com.example.VEM.BackStage;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.VEM.R;

public class BackStageActivity extends AppCompatActivity implements BackStageContract.BackStageView{

    private BackStageContract.BackStagePresenter backStagePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_backstage);
        new BackStagePresenter(this);
    }

    @Override
    public void initBackStagePresenter(BackStageContract.BackStagePresenter backStagePresenter){
        this.backStagePresenter = backStagePresenter;
    }


}
