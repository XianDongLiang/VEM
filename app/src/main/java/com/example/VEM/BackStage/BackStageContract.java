package com.example.VEM.BackStage;

public interface BackStageContract {
    interface BackStageView{
        void initBackStagePresenter(BackStagePresenter backStagePresenter);
    }
    interface BackStagePresenter{

    }
}
