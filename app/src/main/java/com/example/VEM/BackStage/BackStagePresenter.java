package com.example.VEM.BackStage;

public class BackStagePresenter implements BackStageContract.BackStagePresenter{

    private BackStageContract.BackStageView backStageView;

    public BackStagePresenter(BackStageContract.BackStageView backStageView){
        this.backStageView = backStageView;
        this.backStageView.initBackStagePresenter(this);
    }

}