package com.example.VEM.Bargain;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.VEM.Constant.Constant;
import com.example.VEM.EventBusEvent.MerchandiseSingleEvent;
import com.example.VEM.EventBusEvent.QRCodeLinkEvent;
import com.example.VEM.QRCode.QRCode;
import com.example.VEM.QRCode.QRCodeContract;
import com.example.VEM.R;
import com.example.VEM.Model.RemoteMerchandise;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.ByteArrayOutputStream;

public class BargainActivity extends AppCompatActivity implements BargainContract.BargainView, View.OnClickListener{

    private BargainContract.BargainPresenter bargainPresenter;

    public BargainActivity(){

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bargain);
        new BargainPresenter(this);
        initBargainView();
    }

    @Override
    public void onStart(){
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop(){
        super.onStop();
        /*移除所有黏性事件*/
        EventBus.getDefault().removeAllStickyEvents();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        bargainPresenter.destroy();
        bargainPresenter = null;
    }

    @Override
    public void initBargainPresenter(BargainContract.BargainPresenter bargainPresenter){
        this.bargainPresenter = bargainPresenter;
    }

    /*根据支付方式生成二维码*/
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(QRCodeLinkEvent event){
        String method = event.obtainMethod();
        String link = event.obtainLink();
        if (method.equals(Constant.ALIPAY_METHOD)){
            createAlipayCode(link);
        }else if (method.equals(Constant.WECHATPAY_METHOD)){
            createWeChatPayCode(link);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN,sticky = true)
    public void onEvent(MerchandiseSingleEvent event){
        RemoteMerchandise merchandise = event.obtainMerchandise();
        bargainPresenter.installAlipayLink(merchandise);
        bargainPresenter.installWeChatPayLink(merchandise);
        ImageView img_bargain_merchandise_change = findViewById(R.id.img_bargain_merchandise_change);
        TextView tv_bargain_merchandise_change = findViewById(R.id.tv_bargain_merchandise_change);
        TextView tv_bargain_price_change = findViewById(R.id.tv_bargain_price_change);
        TextView tv_bargain_storage_change = findViewById(R.id.tv_bargain_storage_change);
        TextView tv_bargain_slot_change = findViewById(R.id.tv_bargain_slot_change);
        if (merchandise != null){
            Glide.with(this).load(merchandise.obtainImageFolderPath()).into(img_bargain_merchandise_change);
            tv_bargain_merchandise_change.setText(String.valueOf(merchandise.obtainMerchandiseName()));
            tv_bargain_price_change.setText(String.valueOf(merchandise.obtainPrice()));
            tv_bargain_storage_change.setText(String.valueOf(merchandise.obtainRemain()));
            tv_bargain_slot_change.setText(String.valueOf(merchandise.obtainSlot()));
        }else {
            /*---加载默认信息---*/
            tv_bargain_merchandise_change.setText(R.string.tv_bargain_merchandise_name_change);
            tv_bargain_price_change.setText(R.string.tv_bargain_merchandise_price_change);
            tv_bargain_storage_change.setText(R.string.tv_bargain_merchandise_storage_change);
            tv_bargain_slot_change.setText(R.string.tv_bargain_merchandise_slot_change);
        }
    }

    private void createAlipayCode(String link){
        ImageView img_bargain_ali_pay = findViewById(R.id.img_bargain_ali_pay);
        QRCodeContract contract = new QRCode();
        if (link != null){
            Bitmap bitmap = contract.generateQRCode(link,180,180);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
            byte[] bytes=outputStream.toByteArray();
            Glide.with(this).load(bytes).into(img_bargain_ali_pay);
        }else {
            /*TODO:加载默认图片并防止内存泄漏*/
            img_bargain_ali_pay.setImageBitmap(null);
            Log.e("BargainActivity","加载支付宝二维码失败");
        }
    }

    private void createWeChatPayCode(String link){
        ImageView img_bargain_wechat_pay = findViewById(R.id.img_bargain_wechat_pay);
        QRCodeContract contract = new QRCode();
        if (link != null){
            Bitmap bitmap = contract.generateQRCode(link,180,180);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
            byte[] bytes=outputStream.toByteArray();
            Glide.with(this).load(bytes).into(img_bargain_wechat_pay);
        }else {
            /*TODO:加载默认图片并防止内存泄漏*/
            img_bargain_wechat_pay.setImageBitmap(null);
            Log.e("BargainActivity","加载微信支付二维码失败");
        }
    }

    private void initBargainView(){
        Button btn_bargain_back_pressed = findViewById(R.id.btn_bargain_back_pressed);
        btn_bargain_back_pressed.setOnClickListener(this);
    }

    @Override
    public void onClick(View view){
        switch (view.getId()){
            case R.id.btn_bargain_back_pressed:{
                finish();
            }break;
            default:{

            }break;
        }
    }
}
