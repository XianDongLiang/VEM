package com.example.VEM.Bargain;

import com.example.VEM.Constant.Constant;
import com.example.VEM.Model.RemoteMerchandise;
import com.example.VEM.OKHttpCallBack.OKHttpUtil;
import com.example.VEM.OKHttpCallBack.QRCodeCallBack;

public class BargainPresenter implements BargainContract.BargainPresenter{

    private BargainContract.BargainView bargainView;

    public BargainPresenter(BargainContract.BargainView bargainView){
        this.bargainView = bargainView;
        this.bargainView.initBargainPresenter(this);
    }

    @Override
    public void installWeChatPayLink(RemoteMerchandise remoteMerchandise){
        String address = Constant.WECHAT_PAY_ADDRESS + processRemoteMerchandise(remoteMerchandise);
        QRCodeCallBack callback = new QRCodeCallBack(Constant.WECHATPAY_METHOD);
        OKHttpUtil.getInstance().requestServer(address,callback);
    }

    @Override
    public void installAlipayLink(RemoteMerchandise remoteMerchandise){
        String address = Constant.ALI_PAY_ADDRESS + processRemoteMerchandise(remoteMerchandise);
        QRCodeCallBack callBack = new QRCodeCallBack(Constant.ALIPAY_METHOD);
        OKHttpUtil.getInstance().requestServer(address,callBack);
    }

    @Override
    public void destroy(){
        bargainView = null;
    }

    private String processRemoteMerchandise(RemoteMerchandise merchandise){
        StringBuilder builder = new StringBuilder();
        builder.append("?").append("VEMUID=").append(merchandise.obtainVEMUID())
                .append("&").append("merchandiseID=").append(merchandise.obtainMerchandiseUID())
                .append("&").append("merchandisePrice=").append(merchandise.obtainPrice())
                .append("&").append("merchandiseNumber=").append("1");
        return builder.toString();
    }

}
