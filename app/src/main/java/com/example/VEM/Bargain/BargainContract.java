package com.example.VEM.Bargain;

import com.example.VEM.Model.RemoteMerchandise;

public interface BargainContract {
    interface BargainView{
        void initBargainPresenter(BargainPresenter bargainPresenter);
    }
    interface BargainPresenter{
        void installWeChatPayLink(RemoteMerchandise merchandise);
        void installAlipayLink(RemoteMerchandise merchandise);
        void destroy();
    }
}
