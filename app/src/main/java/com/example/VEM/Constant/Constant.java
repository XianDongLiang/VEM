package com.example.VEM.Constant;

public class Constant {

    /*----生成微信二维码支付链接地址----*/
    public static final String WECHAT_PAY_ADDRESS = "http://47.106.112.193/public/index.php/user/Wechat";
    /*----生成支付宝二维码支付链接地址----*/
    public static final String ALI_PAY_ADDRESS = "http://47.106.112.193/public/index.php/user/Alipay";
    /*----微信支付----*/
    public static final String WECHATPAY_METHOD = "WeChat";
    /*----支付宝支付----*/
    public static final String ALIPAY_METHOD = "Ali";
    /*服务器更新查询路径*/
    public static final String UPDATE_QUERY_URL = "";
    /*服务器更新重置路径*/
    public static final String RESET_QUERY_URL = "";
    /*服务器商品列表下载路径*/
    public static final String MERCHANDISE_DOWNLOAD_URL = "http://47.106.112.193:80/public/index.php/user/goodsapi?vendoruuid=321";
    /*本地商品图片文件夹路径*/
    public static final String MERCHANDISE_FOLDER_PATH = "/storage/emulated/0/RemoteMerchandise";
    /*服务器广告列表下载路径*/
    public static final String ADVERTISEMENT_DOWNLOAD_URL = "http://47.106.112.193:80/public/index.php/user/Goodsadd";
    /*本地广告图片文件夹路径*/
    public static final String ADVERTISEMENT_FOLDER_PATH = "/storage/emulated/0/RemoteAdvertisement";
    /*服务器application下载路径*/
    public static final String APP_DOWNLOAD_URL = "";
    /*本地application名称*/
    public static final String APP_NAME = "VEM.apk";
    /*本地application文件夹路径*/
    public static final String APP_FOLDER_PATH = "/storage/emulated/0/Application";
    /*EventBus商品索引*/
    public static final int MERCHANDISE_INDEX = 0x01;
    /*EventBus广告索引*/
    public static final int ADVERTISEMENT_INDEX = 0x02;
    /*EventBusAPP索引*/
    public static final int APP_INDEX = 0x03;
    /*EventBus重启索引*/
    public static final int RESTART_INDEX = 0x04;
    /*自定义广播*/
    public static final String SMS_SEND_ACTION = "android.provider.Telephony.SMS_SEND";
    /*Android*/
    public static final String SMS_RECEIVED_ACTION = "android.provider.Telephony.SMS_RECEIVED";
    /*Android*/
    public static final String SMS_DELIVER_ACTION = "android.provider.Telephony.SMS_DELIVER";
    /*Android开机广播*/
    public static final String BOOT_COMPLETED_ACTION = "android.intent.action.BOOT_COMPLETED";
    /*重启VEM开始时间*/
    public static final String START_TIME = "";
    /*重启VEM结束时间*/
    public static final String END_TIME = "";
    /*中国移动运营商代号*/
    public static final String CMCC = "CMCC";
    /*中国移动服务热线*/
    public static final String CMCC_NUMBER = "10086";
    /*中国移动查询余额*/
    public static final String CMCC_CXYE = "YE";
    /*中国移动查询当月套餐剩余短信条数*/
    public static final String CMCC_CXBX = "CXBX";
    /*中国移动查询当月流量*/
    public static final String CMCC_CXLL = "CXLL";
    /*中国联通运营商代号*/
    public static final String CUCC = "CUCC";
    /*中国联通服务热线*/
    public static final String CUCC_NUMBER = "10010";
    /*中国电信运营商代号*/
    public static final String CTCC = "CTCC";
    /*中国电信服务热线*/
    public static final String CTCC_NUMBER = "";
    /**/
    public static final String SERIAL_PORT_PATH = "/system/bin/su";
    /*波特率*/
    public static final int B50 = 50;
    public static final int B75 = 75;
    public static final int B110 = 110;
    public static final int B134 = 134;
    public static final int B150 = 150;
    public static final int B200 = 200;
    public static final int B300 = 300;
    public static final int B600 = 600;
    public static final int B1200 = 1200;
    public static final int B1800 = 1800;
    public static final int B2400 = 2400;
    public static final int B4800 = 4800;
    public static final int B9600 = 9600;
    public static final int B19200 = 19200;
    public static final int B38400 = 38400;
    public static final int B57600 = 57600;
    public static final int B115200 = 115200;
    public static final int B230400 = 230400;
    public static final int B460800 = 460800;
    public static final int B500000 = 500000;
    public static final int B576000 = 576000;
    public static final int B921600 = 921600;
    public static final int B1000000 = 1000000;
    public static final int B1152000 = 1152000;
    public static final int B1500000 = 1500000;
    public static final int B2000000 = 2000000;
    public static final int B2500000 = 2500000;
    public static final int B3000000 = 3000000;
    public static final int B3500000 = 3500000;
    public static final int B4000000 = 4000000;
    /*测试驱动板链接*/
    public static final String CMD_SLV_LNK = "CMD_SLV_LNK";
    public static final int LINK_FUNCTION_CODE_EXPAND = 0x28;
    public static final byte[] LINK_CMD = {0x01,0x66, LINK_FUNCTION_CODE_EXPAND,0x0B,(byte)0xBE};
    /*主板状态轮询，获取状态更新标志*/
    public static final String CMD_PC_POLL = "CMD_PC_POLL";
    public static final int STATUS_FUNCTION_CODE_EXPAND = 0x29;
    public static final byte[] STATUS_CMD = {0x01,0x66,STATUS_FUNCTION_CODE_EXPAND,(byte)0xCA,0X7E};
    /*获取主板货道数据*/
    public static final String CMD_PC_SLOT_TO_PC = "CMD_PC_SLOT_TO_PC";
    public static final int SLOT_RECEIVE_FUNCTION_CODE_EXPAND = 0x30;
    /*获取主板货道数据*/
    public static final String CMD_PC_SLOT_TO_SCM = "CMD_PC_SLOT_TO_SCM";
    public static final int SLOT_SEND_FUNCTION_CODE_EXPAND = 0x31;
    /*上位机控制出货*/
    public static final String CMD_PC_TRANSFOR = "CMD_PC_TRANSFER";
    public static final int TRANSFER_SEND_FUNCTION_CODE_EXPAND = 0x32;
    /*主板发送交易到上位机*/
    public static final String CMD_PC_SEND_TRADE = "CMD_PC_SEND_TRADE";
    public static final int TRANSFER_RECEIVE_FUNCTION_CODE_EXPAND = 0x33;
    public static final byte[] TRANSFER_RECEIVE_CMD = {0x01,0x66,TRANSFER_RECEIVE_FUNCTION_CODE_EXPAND,0x4B,(byte) 0xB5};
    /*上位机同步主板时间*/
    public static final String CMD_PC_SYNC_TIME = "CMD_PC_SYNC_TIME";
    public static final int SYNC_TIME_FUNCTION_CODE_EXPAND = 0x34;
    /*上位机清除上货事件标志*/
    public static final String CMD_PC_RELOAD_OK = "CMD_PC_RELOAD_OK";
    public static final int SLOT_RELOAD_FUNCTION_CODE_EXPAND = 0x35;
    public static final byte[] SLOT_RELOAD_CMD = {0x01,0x66,SLOT_RELOAD_FUNCTION_CODE_EXPAND,(byte)0xCB,(byte)0xB7};
    /*上位机通知主板写货道数据到存储器*/
    public static final String CMD_PC_WRT_SLOT = "CMD_PC_WRT_SLOT";
    public static final int SLOT_WRITE_FUNCTION_CODE_EXPAND = 0x36;
    public static final byte[] SLOT_WRITE_CMD = {0x01,0x66,SLOT_WRITE_FUNCTION_CODE_EXPAND,(byte)0x8B,(byte)0xB6};

}
