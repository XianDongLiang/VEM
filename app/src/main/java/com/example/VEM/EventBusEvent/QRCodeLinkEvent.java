package com.example.VEM.EventBusEvent;

public class QRCodeLinkEvent {

    /*支付方式*/
    private String method;
    /*支付链接*/
    private String link;

    public QRCodeLinkEvent(String method,String link){
        this.method = method;
        this.link = link;
    }

    public String obtainMethod(){
        return method;
    }

    public String obtainLink(){
        return link;
    }
}
