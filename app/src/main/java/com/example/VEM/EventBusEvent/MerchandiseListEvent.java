package com.example.VEM.EventBusEvent;

import com.example.VEM.Model.RemoteMerchandise;

import java.util.List;

public class MerchandiseListEvent {

    private List<RemoteMerchandise> merchandiseList;

    public MerchandiseListEvent(List<RemoteMerchandise> merchandiseList){
        this.merchandiseList = merchandiseList;
    }

    public List<RemoteMerchandise> obtainMerchandiseList(){
        return merchandiseList;
    }
}
