package com.example.VEM.EventBusEvent;

import com.example.VEM.Model.RemoteMerchandise;

public class MerchandiseSingleEvent {

    /*商品信息传递*/
    private RemoteMerchandise merchandise;

    public MerchandiseSingleEvent(RemoteMerchandise merchandise){
        this.merchandise = merchandise;
    }

    public RemoteMerchandise obtainMerchandise() {
        return merchandise;
    }
}
