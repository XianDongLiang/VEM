package com.example.VEM.EventBusEvent;

import com.example.VEM.Model.RemoteAdvertisement;

import java.util.List;

public class AdvertisementListEvent {

    private List<RemoteAdvertisement> advertisementList;

    public AdvertisementListEvent(List<RemoteAdvertisement> advertisementList){
        this.advertisementList = advertisementList;
    }

    public List<RemoteAdvertisement> obtainAdvertisementList(){
        return advertisementList;
    }
}
