package com.example.VEM.Service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import com.example.VEM.Constant.Constant;

public class SMSReceiver extends BroadcastReceiver {

    public void SMSReceiver(){

    }

    @Override
    public void onReceive(Context context, Intent intent){

        if (intent.getAction() != null && intent.getAction().equals(Constant.SMS_RECEIVED_ACTION)){
            receiveSMSMessage(context,intent);
        }else if (intent.getAction() != null && intent.getAction().equals(Constant.SMS_DELIVER_ACTION)){

        }else if (intent.getAction() != null && intent.getAction().equals(Constant.SMS_SEND_ACTION)){

        }
    }

    private void receiveSMSMessage(Context context,Intent intent){
        Bundle bundle = intent.getExtras();
        Object[] PDUS = (Object[]) bundle.get("pdus");
        if (PDUS != null &&PDUS.length > 0){
            SmsMessage[] messages = new SmsMessage[PDUS.length];
            for (int i=0 ; i < PDUS.length ;i++){
                byte[] temp = (byte[]) PDUS[i];
                messages[i] = SmsMessage.createFromPdu(temp);
            }
            for (SmsMessage message : messages){
                String content = message.getMessageBody();
                String sender = message.getOriginatingAddress();
                Log.d("SMSReceiver",content);
            }
        }
    }

}
