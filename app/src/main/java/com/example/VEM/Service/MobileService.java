package com.example.VEM.Service;

import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.telephony.SmsManager;

import com.example.VEM.Constant.Constant;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class MobileService extends Service {

    private SMSObserver observer;

    private SMSReceiver sendSMSReceiver;
    private SMSReceiver receiveSMSReceiver;
    private SMSReceiver deliverSMSReceiver;

    public MobileService() {

    }

    @Override
    public void onCreate(){
        super.onCreate();
        EventBus.getDefault().register(this);
        registerService();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        unregisterService();
        stopSelf();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(JSONObject event){

    }

    /*----注册广播----*/
    private void registerService(){
        IntentFilter receiveFilter = new IntentFilter(Constant.SMS_RECEIVED_ACTION);
        receiveSMSReceiver = new SMSReceiver();
        registerReceiver(receiveSMSReceiver,receiveFilter);
        IntentFilter sendFilter = new IntentFilter(Constant.SMS_SEND_ACTION);
        sendSMSReceiver = new SMSReceiver();
        registerReceiver(sendSMSReceiver,sendFilter);
        IntentFilter deliverFilter = new IntentFilter(Constant.SMS_DELIVER_ACTION);
        deliverSMSReceiver = new SMSReceiver();
        registerReceiver(deliverSMSReceiver,deliverFilter);
    }

    /*----解除广播----*/
    private void unregisterService(){
        if (receiveSMSReceiver != null){
            unregisterReceiver(receiveSMSReceiver);
        }
        if (sendSMSReceiver != null){
            unregisterReceiver(sendSMSReceiver);
        }
        if (deliverSMSReceiver != null){
            unregisterReceiver(deliverSMSReceiver);
        }
    }

    /*来自阿里巴巴Android开发手册*/

    /*
    * 新建线程时，必须通过线程池提供（AsyncTask 或者 ThreadPoolExecutor或者其他形式自定义的线程池），不允许在应用中自行显式创建线程。
    * 说明：
    * 使用线程池的好处是减少在创建和销毁线程上所花的时间以及系统资源的开销，解决资源不足的问题。如果不使用线程池，有可能造成系统创建大量同类线程而导致
    * 消耗完内存或者“过度切换”的问题。另外创建匿名线程不便于后续的资源使用分析，
    * 对性能分析等会造成困扰。
    * */

    /*
    * 线程池不允许使用 Executors 去创建，而是通过 ThreadPoolExecutor 的方
    * 式，这样的处理方式让写的同学更加明确线程池的运行规则，规避资源耗尽的风险。
    * 说明：
    * Executors 返回的线程池对象的弊端如下：
    * 1) FixedThreadPool 和 SingleThreadPool ： 允许的请求队列长度为Integer.MAX_VALUE，可能会堆积大量的请求，从而导致 OOM；
    * 2) CachedThreadPool 和 ScheduledThreadPool ： 允许的创建线程数量为Integer.MAX_VALUE，可能会创建大量的线程，从而导致 OOM。
    * */


    /*发送短信进行查询流量、话费*/
    private void sendSMSMessage(String address,String CXLL,String CXYE,String CXBX){
        SmsManager smsManager = SmsManager.getDefault();
        Intent sendIntent = new Intent(Constant.SMS_SEND_ACTION);
        Intent deliveredIntent = new Intent(Constant.SMS_DELIVER_ACTION);
        PendingIntent sendPI = PendingIntent.getBroadcast(this,0,sendIntent,0);
        PendingIntent deliveredPI = PendingIntent.getBroadcast(this,0,deliveredIntent,0);
        /*发送查询流量短信*/
        smsManager.sendTextMessage(address,null,CXLL,sendPI,deliveredPI);
        /*发送查询话费短信*/
        smsManager.sendTextMessage(address,null,CXYE,sendPI,deliveredPI);
        /*发送查询短信剩余短信*/
        // smsManager.sendTextMessage(address,null,CXBX,sendPI,deliveredPI);
    }

    private void observerDatabase(){
        ContentResolver contentResolver = getContentResolver();
        observer = new SMSObserver(contentResolver,new SMSHandler());
        contentResolver.registerContentObserver(Uri.parse("content://sms"),true,observer);
    }

    private class SMSObserver extends ContentObserver {

        private ContentResolver contentResolver;
        private Handler handler;

        public SMSObserver(ContentResolver contentResolver, Handler handler){
            super(handler);
            this.contentResolver = contentResolver;
            this.handler = handler;
        }

        @Override
        public void onChange(boolean selfChange){
            super.onChange(selfChange);
            String[] projection = new String[]{"_id","address","person","body","date","type"};
            Cursor cursor = contentResolver.query(Uri.parse("content://sms/inbox"),projection,null,null,"date desc");
            if (cursor == null){
                return;
            }
            while (cursor.moveToNext()){
                String number = cursor.getString(cursor.getColumnIndex("address"));
                String name = cursor.getString(cursor.getColumnIndex("person"));
                String body = cursor.getString(cursor.getColumnIndex("body"));
            }
            cursor.close();
        }
    }

    private class SMSHandler extends Handler{

    }
}
