package com.example.VEM.Service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.VEM.Constant.Constant;
import com.example.VEM.Main.Main.MainActivity;

public class BootStartReceiver extends BroadcastReceiver {

    public BootStartReceiver(){

    }

    @Override
    public void onReceive(Context context, Intent intent){

        if (intent.getAction() != null && intent.getAction().equals(Constant.BOOT_COMPLETED_ACTION)){
            /*开机自启动MainActivity*/
            Intent startAppIntent = new Intent(context,MainActivity.class);
            startAppIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(startAppIntent);
            /*开机自启动MobileService*/
            //Intent startServiceIntent = new Intent(context,MobileService.class);
            //context.startService(startServiceIntent);
        }
    }

}
